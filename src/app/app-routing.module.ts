import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoPreloading, RouterModule, Routes } from '@angular/router';

import { MainComponent } from './pages/main.component';
import { FaqComponent } from './shared/faq/faq.component';
import { LiveChatComponent } from './shared/live-chat/live-chat.component';
import { MyProfileComponent } from './shared/my-profile/my-profile.component';

import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { ForbiddenComponent } from './pages/forbidden/forbidden.component';

const routes: Routes = [
  { path:  '', redirectTo: '/auth/login', pathMatch: 'full'},
  { path:  'auth', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) },
  { path:  'faq', component: FaqComponent},
  { path:  '', component: MainComponent, children: [
    { path:  'administration', loadChildren: () => import('./pages/administration/administration.module').then(m => m.AdministrationModule) },
    { path:  'delivery', loadChildren: () => import('./pages/delivery-man/delivery-man.module').then(m => m.DeliveryManModule) },
    { path:  'business', loadChildren: () => import('./pages/business/business.module').then(m => m.BusinessModule) },
    { path: 'forbidden', component: ForbiddenComponent},
    { path: 'live-chat', component: LiveChatComponent},
    { path: 'my-profile', component: MyProfileComponent}
  ]},
  { path: '**', redirectTo: '/auth/login', pathMatch: 'full'}
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, { preloadingStrategy: NoPreloading }),
    LoadingBarRouterModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
