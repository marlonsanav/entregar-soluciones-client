import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  subscription: Subscription = new Subscription();
  loading: boolean = false;
  userId: any;
  passwordTextType: any;

  regexValidationEmail = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

  loginForm = new FormGroup({
    emailUser: new FormControl(''),
    passwordUser: new FormControl('')
  });

  constructor(private _loadingBarService: LoadingBarService, private _authSvc: AuthenticationService, private _router: Router) {
    if (localStorage.getItem('user_id')) {
      this.userId = localStorage.getItem('user_id')
      this.redirectLoggedByUserRole(this.userId);
    }    
  }

  ngOnInit(): void {
  }
  
  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  togglePasswordTextType(){
    this.passwordTextType = !this.passwordTextType;
  }

  async onSubmit() {

    this.startBarCharge();
    if(this.loginForm.value.emailUser.indexOf('@') != -1){
      await this._authSvc.login(this.loginForm.value.emailUser, this.loginForm.value.passwordUser)
        .then(() => this.redirectLoggedByUserRole(this._authSvc.userId))
        .catch((err) => {
          this.completeBarCharge();
          console.log("Error de Inicio de Sesión: " + err);
          Swal.fire({
            icon: 'error',
            title: '¡Ups...!',
            text: '¡Parece que los datos son erróneos!'
          });
        });
    }
    else{
      await this._authSvc.loginWithClickLogin(this.loginForm.value.emailUser, this.loginForm.value.passwordUser)
      .then(() => this.redirectLoggedByUserRole(this._authSvc.userId))
      .catch((err:any) => {
        this.completeBarCharge();
        console.log("Error de Inicio de Sesión: " + err);
        Swal.fire({
          icon: 'error',
          title: '¡Ups...!',
          text: '¡Parece que los datos son erróneos!'
        });
      });
    }
  }

  redirectLoggedByUserRole(userId: string) {
    this.subscription = this._authSvc.getRole(userId)
      .subscribe(data => {
        let user = data;
        user.forEach(((element: any) => {
          var dataQuery = element;
          user = dataQuery['profile_ID'];
        }));
        var userRole = JSON.stringify(user);
        userRole = this._authSvc.toStringRolesAndRoutes(userRole);

        if (userRole == "administrator") {
          this._router.navigate(['/delivery/dashboard']);
        }
        else if (userRole == "delivery-man") {
          this._router.navigate(['/delivery/dashboard']);
        }
        else if (userRole == "business") {
          this._router.navigate(['/business/dashboard']);
        }

        this.completeBarCharge();
      });


  }


  startBarCharge() {
    this._loadingBarService.start();
    this.loading = true;
  }

  completeBarCharge() {
    this._loadingBarService.complete();
    this.loading = false;
  }

}
