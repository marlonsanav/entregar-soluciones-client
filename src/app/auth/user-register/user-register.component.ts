import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { UsersService } from 'src/app/services/users.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Observable, Observer } from 'rxjs';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent {

  loading: boolean = false;

  isMatch: boolean = false;

  regexValidationEmail = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
  regexValidationPass = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/;

  companyRegisterForm = new FormGroup({
    identificationCompany: new FormControl(0, Validators.compose([Validators.min(10000000), Validators.max(9999999999), Validators.required])),
    nameCompany: new FormControl('', Validators.compose([Validators.minLength(3), Validators.maxLength(50), Validators.required])),
    addressCompany: new FormControl('', Validators.compose([Validators.minLength(3), Validators.maxLength(75), Validators.required])),
    phoneCompany: new FormControl(0, Validators.compose([Validators.min(1000000), Validators.max(9999999999), Validators.required])),
    emailCompany: new FormControl('', Validators.pattern(this.regexValidationEmail)),
    passwordCompany: new FormControl('', Validators.pattern(this.regexValidationPass)),
    confirmPasswordCompany: new FormControl('', Validators.required)
  });

  userRegisterForm = new FormGroup({
    typeDocumentUser: new FormControl(),
    identificationUser: new FormControl(0, Validators.compose([Validators.min(10000000), Validators.max(9999999999), Validators.required])),
    nameUser: new FormControl('', Validators.compose([Validators.minLength(3), Validators.maxLength(50), Validators.required])),
    addressUser: new FormControl('', Validators.compose([Validators.minLength(3), Validators.maxLength(75), Validators.required])),
    phoneUser: new FormControl(0, Validators.compose([Validators.min(1000000), Validators.max(9999999999), Validators.required])),
    emailUser: new FormControl('', Validators.pattern(this.regexValidationEmail)),
    motoUser: new FormControl(),
    passwordUser: new FormControl('', Validators.pattern(this.regexValidationPass)),
    confirmPasswordUser: new FormControl('', Validators.required)
  });


  itemsComments = [
    {name: "CC", value: "CC"},
    {name: "CE", value: "CE"},
  ]

  constructor(private _usersSvc: UsersService, private _router: Router, private _loadingBarService: LoadingBarService) { }


  onSubmitUser() {
    this.startBarCharge();
    const userRegisterData = this.userRegisterForm.value;

    if(this.userRegisterForm.status == 'VALID'){
      this._usersSvc.createUser(userRegisterData)
        .then(() => {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Usuario creado en la aplicación',
            footer: 'Puede ingresar con los datos suministrados en el formulario anterior.'
          });
          this.completeBarCharge();
          this._router.navigate(['/login']);
        })
        .catch((err) => {
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Problemas al enviar',
            showConfirmButton: false,
            timer: 1500
          });
          this.completeBarCharge();
        });
    }
    else{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Verifique los campos del formulario',
        showConfirmButton: false,
        timer: 1500
      });
      this.completeBarCharge();
    }
    
  }

  onSubmitCompany() {
    this.startBarCharge();
    const companyRegisterData = this.companyRegisterForm.value;
    if(this.companyRegisterForm.status == 'VALID'){
      this._usersSvc.createCompany(companyRegisterData)
        .then(() => {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Usuario creado en la aplicación',
            footer: 'Puede ingresar con los datos suministrados en el formulario anterior.'
          });
          this.completeBarCharge();
          this._router.navigate(['/login']);
        })
        .catch((err) => {
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Problemas al crear el usuario',
            showConfirmButton: false,
            timer: 1500
          });
          this.completeBarCharge();
        });
    }
    else{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Verifique los campos del formulario',
        showConfirmButton: false,
        timer: 1500
      });
      this.completeBarCharge();
    }
    
  }  

  validatePasswordCompany(event: any) {
    let passwordValue = this.companyRegisterForm.controls.passwordCompany.value;
    if (passwordValue == event.target.value) {
      this.isMatch = true;
      this.companyRegisterForm.controls.confirmPasswordCompany.setErrors(null);
    }
    else {
      this.isMatch = false;
      this.companyRegisterForm.controls.confirmPasswordCompany.setErrors({ 'incorrect': true });
    }
  }  

  validatePasswordUser(event: any) {
    let passwordValue = this.companyRegisterForm.controls.passwordUser.value;
    if (passwordValue == event.target.value) {
      this.isMatch = true;
      this.userRegisterForm.controls.confirmPasswordUser.setErrors(null);
    }
    else {
      this.isMatch = false;
      this.userRegisterForm.controls.confirmPasswordUser.setErrors({ 'incorrect': true });
    }
  }  

  startBarCharge() {
    this._loadingBarService.start();
    this.loading = true;
  }

  completeBarCharge() {
    this._loadingBarService.complete();
    this.loading = false;
  }
}
