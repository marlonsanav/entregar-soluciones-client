import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthenticationGuard } from 'src/app/guard/authentication.guard';

import { UsersListComponent } from './users-list/users-list.component';
import { UsersDetailsComponent } from './users-details/users-details.component';
import { CreateUsersComponent } from './create-users/create-users.component';
import { EditUsersComponent } from './edit-users/edit-users.component';

const routes: Routes = [
  { path: '', redirectTo: 'users-list' },
  { path: 'users-list', component: UsersListComponent, canActivate: [AuthenticationGuard], data: {role: "administrator"} },   
  { path: 'create-users', component: CreateUsersComponent, canActivate: [AuthenticationGuard], data: {role: "administrator"} },
  { path: 'edit-users/:id', component: EditUsersComponent, canActivate: [AuthenticationGuard], data: {role: "administrator"}},
  { path: 'users-details/:id', component: UsersDetailsComponent, canActivate: [AuthenticationGuard], data: {role: "administrator"} }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrationRoutingModule { }
