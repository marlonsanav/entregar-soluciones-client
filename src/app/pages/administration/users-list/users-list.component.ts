import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import Swal from 'sweetalert2';
import { UsersService } from '../../../services/users.service';
import { AuthenticationService } from '../../../services/authentication.service';
import { map } from 'rxjs/operators';

export interface UserInterface {
  user_ID?: string;
  auth_ID: string;
  names: string;
  surnames: string;
  email: string;
  phone: string;
  profile_ID: string;
  state: string;
  type_ID: string;
  location: string;
}

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit, AfterViewInit {

  loading: boolean = false;

  loadingData: boolean = true;

  displayedColumns = ['names', 'role', 'actions'];
  
  dataSourceX = new MatTableDataSource();

  isToggleAccess: boolean = false;

  toggleAccessText: string = "Aprobar Usuarios";

  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  @ViewChild(MatSort, { static: true })
  sort!: MatSort;

  constructor(private _usersSvc: UsersService, private _authSvc: AuthenticationService) { }


  ngOnInit(): void {
    this.getUsers();
  }

  ngAfterViewInit() {     
    this.dataSourceX.paginator = this.paginator;
    this.dataSourceX.paginator._intl.itemsPerPageLabel = 'Items por página';
    this.dataSourceX.paginator._intl.nextPageLabel = 'Siguiente';
    this.dataSourceX.paginator._intl.previousPageLabel = 'Anterior';
    this.dataSourceX.paginator._intl.firstPageLabel = "Primera Página";
    this.dataSourceX.paginator._intl.lastPageLabel = "Última Página";
    this.dataSourceX.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length == 0 || pageSize == 0) {
        return `0 de ${length}`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      // If the start index exceeds the list length, do not try and fix the end index to the end.
      const endIndex = startIndex < length ?
        Math.min(startIndex + pageSize, length) :
        startIndex + pageSize;
      return `${startIndex + 1} – ${endIndex} de ${length}`;
    }
    this.dataSourceX.sort = this.sort;
  }

  getUsers() {
    this._usersSvc.getAllUsers().pipe(
      map(actions => {
        return actions.map(
          (data:any) => ({
            userId: data.payload.doc.id,
            names: data.payload.doc.data().names,
            email: data.payload.doc.data().email,
            identification: data.payload.doc.data().identification,
            phone: data.payload.doc.data().phone,
            status: (data.payload.doc.data().status == 1) ? "Activo" : "Inactivo",
            profile_ID: this._authSvc.toStringRoles(data.payload.doc.data().profile_ID.toString())
          }));
      }))
      .subscribe( (data) => {
        this.loadingData = false;
        this.dataSourceX.data = data;
        console.log(data);
      } 
    );    
  }

  onDeleteUser($event: any, user: any) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: true
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estás segur@?',
      text: "¡No podrás revertir este cambio!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar',
      cancelButtonText: 'No, no eliminar',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this._usersSvc.deleteUser(user);
        swalWithBootstrapButtons.fire(
          '¡Eliminado!',
          'El usuario ha sido eliminado.',
          'success'
        );
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'El usuario esta a salvo :)',
          'error'
        )
      }
    })
  }

  applyFilter(filterValue: any) {
    this.dataSourceX.filter = filterValue.target.value.trim().toLowerCase();
  }

  exportCsv() {
    var data = this.dataSourceX.data;
    var array = typeof data != 'object' ? JSON.parse(data) : data;
    var str = '';
    var column = 'ID, Nombre Completo, Correo, Identificación, Teléfono, Estado, Perfil \r\n';
    str += column;
    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if (line != '') line += ','
            line += array[i][index];
        }

        str += line + '\r\n';
    }
    var dateCsv = new Date();
    var yearCsv = dateCsv.getFullYear();
    var monthCsv = (dateCsv.getMonth() + 1 <= 9) ? '0' + (dateCsv.getMonth() + 1) : (dateCsv.getMonth() + 1);
    var dayCsv = (dateCsv.getDate() <= 9) ? '0' + dateCsv.getDate() : dateCsv.getDate();
    var fullDateCsv = yearCsv + "-" + monthCsv + "-" + dayCsv;


    var blob = new Blob(["\ufeff" + str], {type: 'type: "text/csv;charset=UTF-8"'});
    var elementToClick = window.document.createElement("a");
    elementToClick.href = window.URL.createObjectURL(blob);
    elementToClick.download = "Usuario-Matriculados-" + fullDateCsv + ".csv";
    elementToClick.click();  
  }

  toggleAccessOptions(){
    this.isToggleAccess = (this.isToggleAccess) ? false : true;
    this.toggleAccessText = (this.isToggleAccess) ? "Listar Usuarios" : "Aprobar Usuarios";
  }

  onChangeUserStatus(userId: string, status: string){
    this._usersSvc.changeUserStatus(userId, status)
      .then((res) => res.json())
      .then((res) => {
        Swal.fire(
          'Confirmado',
          res,
          'success'
        );
      }).catch((error) => {
        Swal.fire(
          'Problemas',
          'La solicitud no fue realizada debido a problemas',
          'error'
        )  
      });
  }

}
