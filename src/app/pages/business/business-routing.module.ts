import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardBusinessComponent } from './dashboard/dashboard.component';
import { OrdersRecordComponent } from './orders-record/orders-record.component';
import { OrdersRequestComponent } from './orders-request/orders-request.component';
import { AuthenticationGuard } from 'src/app/guard/authentication.guard';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard'},
  { path: 'dashboard', component: DashboardBusinessComponent, canActivate: [AuthenticationGuard], data: {role: "business"} },
  { path: 'record', component: OrdersRecordComponent, canActivate: [AuthenticationGuard], data: {role: "business"} },
  { path: 'request', component: OrdersRequestComponent, canActivate: [AuthenticationGuard], data: {role: "business"} },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessRoutingModule { }
