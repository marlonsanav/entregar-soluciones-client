import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardBusinessComponent } from './dashboard/dashboard.component';
import { OrdersRecordComponent } from './orders-record/orders-record.component';
import { OrdersRequestComponent } from './orders-request/orders-request.component';


import { MaterialModule } from '../../utils/material/material.module';
import { ReactiveFormsModule  } from '@angular/forms';
import { LoadingBarModule } from '@ngx-loading-bar/core';

import { BusinessRoutingModule } from './business-routing.module';

@NgModule({
  declarations: [
    DashboardBusinessComponent,
    OrdersRecordComponent,
    OrdersRequestComponent
  ],
  imports: [
    CommonModule,
    BusinessRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    LoadingBarModule
  ]
})
export class BusinessModule { }
