import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { DeliveriesService } from 'src/app/services/deliveries.service';
import { map } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { UsersService } from 'src/app/services/users.service';
import { Subscription } from 'rxjs';

export interface ContingencyInterface {
  contingency_ID?: string,
  user_ID: string;
  contingency_Type: string;
  contingency_State: string;
  contingency_Date: Date;
  answer: string;
  details: string;
  reference: string;
}

@Component({
  selector: 'app-dashboard-business',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardBusinessComponent implements OnInit, OnDestroy, AfterViewInit {

  loading: boolean = false;

  loadingData: boolean = true;

  userId: string = '';

  user: any = {
    names: ""
  };

  subscriptionDeliveries: Subscription = new Subscription();

  paginatorIntl = new MatPaginatorIntl();

  displayedColumns = ['company_name', 'product_name', 'product_price'];

  dataSourceX = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  @ViewChild(MatSort, { static: true })
  sort!: MatSort;

  constructor(private _router: Router, private _deliveriesSvc: DeliveriesService, private _usersSvc: UsersService) {
  }

  ngOnInit(): void {
    this.userId = localStorage.getItem('user_id') || '';
    this.getDeliveries();
    if(localStorage.getItem('user_names')){
      this.user.names = localStorage.getItem('user_names');
    }
  }

  ngOnDestroy(){
    this.subscriptionDeliveries.unsubscribe();
  }

  ngAfterViewInit() {
    this.dataSourceX.paginator = this.paginator;
    this.dataSourceX.paginator._intl.itemsPerPageLabel = 'Items por página';
    this.dataSourceX.paginator._intl.nextPageLabel = 'Siguiente';
    this.dataSourceX.paginator._intl.previousPageLabel = 'Anterior';
    this.dataSourceX.paginator._intl.firstPageLabel = "Primera Página";
    this.dataSourceX.paginator._intl.lastPageLabel = "Última Página";
    this.dataSourceX.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length == 0 || pageSize == 0) {
        return `0 de ${length}`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      // If the start index exceeds the list length, do not try and fix the end index to the end.
      const endIndex = startIndex < length ?
        Math.min(startIndex + pageSize, length) :
        startIndex + pageSize;
      return `${startIndex + 1} – ${endIndex} de ${length}`;
    }
    this.dataSourceX.sort = this.sort;
  }

  getDeliveries() {
    this.loadingData = true;
    this.subscriptionDeliveries = this._deliveriesSvc.getDeliveriesByCompanyId(this.userId).pipe(
      map(actions => {
        return actions.map(
          (data: any) => ({
            company_name: data.payload.doc.data().company_name,
            delivery_ID: data.payload.doc.id,
            delivery_name: data.payload.doc.data().delivery_name,
            delivery_phone: data.payload.doc.data().delivery_phone,
            delivery_price: new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(data.payload.doc.data().delivery_price),
            delivery_location: data.payload.doc.data().delivery_location,
            delivery_man_phone: data.payload.doc.data().delivery_man_phone,
            details: data.payload.doc.data().details,
            product_name: data.payload.doc.data().product_name,
            product_price: new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(data.payload.doc.data().product_price),
            product_type: data.payload.doc.data().product_type,
            start_location: data.payload.doc.data().start_location,
            start_date: data.payload.doc.data().start_date,
            user_ID: data.payload.doc.data().user_ID,
            status: data.payload.doc.data().status
          }));
      }))
      .subscribe(data => {
        this.loadingData = false;
        this.dataSourceX.data = data
      });
  }

  onCancelRequest(data: any){   
    
    this.generatePushNotification(data.delivery_ID);

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: true
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Cancelar Solicitud',
      text: "¡Al cancelar la solicitud, deberá radicar una nueva para continuar con su entrega!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quiero cancelarla',
      cancelButtonText: 'No, volver',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this._deliveriesSvc.cancelDelivery(data.delivery_ID).then(() => {
          Swal.fire(
            'Confirmado',
            'La solicitud ha sido cancelada',
            'success'
          );                                 
        })
        .catch(() => {
          Swal.fire(
            'Error',
            'Se ha presentado un error al cancelar',
            'error'
          )       
        });
        
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Información',
          'La solicitud continua en pie',
          'info'
        )
      }
    });
  }

  onContact(data: any){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: true
    })

    swalWithBootstrapButtons.fire({
      title: '¡Detalles del Pedido!',
      html: `<p style='font-size: 14px;text-align: justify;'>
             <b>Nombre del Cliente:</b> ${data.delivery_name}
             <br><b>Celular del Cliente:</b> ${data.delivery_phone}
             <br><b>Dirección del Cliente:</b> ${data.delivery_location}<br>
             <br><b>Nombre del Producto:</b> ${data.product_name}
             <br><b>Precio del Producto:</b> ${data.product_price}
             <br><b>Coste de Envío:</b> ${data.delivery_price}</p>`,
      showCancelButton: true,
      confirmButtonText: 'Escribir a Domiciliario',
      cancelButtonText: 'Escribir a Cliente ',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = 'https://api.whatsapp.com/send?phone=+57' + data.delivery_man_phone;
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        window.location.href = 'https://api.whatsapp.com/send?phone=+57' + data.delivery_phone;
      }
    });
  }

  generatePushNotification(deliveryId: string){
    this._usersSvc.getUserByDeliveryId(deliveryId)
      .then((res) => res.json())
      .then((data) => {
        this._deliveriesSvc.sendPushNotification(data.data, "delivery-finish")
            .then(res => res.json())
            .then(data => console.log(data))
            .catch((err) => {
              console.error("Hubo un error")
            });  
      })
      .catch((err) => console.error(err))    
  }

}
