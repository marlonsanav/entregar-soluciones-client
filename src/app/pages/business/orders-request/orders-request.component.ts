import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { DeliveriesService } from 'src/app/services/deliveries.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-orders-request',
  templateUrl: './orders-request.component.html',
  styleUrls: ['./orders-request.component.css']
})

export class OrdersRequestComponent implements OnInit {

  loading: boolean = false;

  requestForm = new FormGroup({
    locationA: new FormControl('', Validators.compose([Validators.minLength(3), Validators.maxLength(50), Validators.required])),
    locationB: new FormControl('', Validators.compose([Validators.minLength(3), Validators.maxLength(50), Validators.required])),
    details: new FormControl('', Validators.compose([Validators.maxLength(150)])),
    productType: new FormControl('', Validators.required),
    productName: new FormControl('', Validators.compose([Validators.minLength(3), Validators.maxLength(50), Validators.required])),
    productPrice: new FormControl(0, Validators.compose([Validators.min(100), Validators.max(9999999999), Validators.required])),
    deliveryPrice: new FormControl(0, Validators.compose([Validators.min(100), Validators.max(9999999999), Validators.required])),
    deliveryName: new FormControl('', Validators.compose([Validators.minLength(3), Validators.maxLength(50), Validators.required])),
    deliveryPhone: new FormControl(0, Validators.compose([Validators.min(1000000), Validators.max(9999999999), Validators.required])),
    userId: new FormControl(),
    companyName: new FormControl(),
    companyPhone: new FormControl()
  });

  // Esto viene desde la BD
  itemsProductType = [
    { name: 'Común', value: 'Comun'},
    { name: 'Especial', value: 'Especial'},
    { name: 'Frágil', value: 'Fragil'},
  ];


  constructor(private _deliveriesSvc: DeliveriesService, private _router: Router, private _loadingBarService: LoadingBarService) { }

  ngOnInit(): void {
    const userId = localStorage.getItem('user_id') || '';
    const userNames = localStorage.getItem('user_names') || '';
    const userLocation = localStorage.getItem('user_location') || '';
    const userPhone = localStorage.getItem('user_phone') || '';
    this.fillRequestForm(userId, userNames, userLocation, userPhone);
  }


  onSubmit() {
    this.startBarCharge();
    this.requestForm.controls.userId.enable();
    this.requestForm.controls.companyName.enable();
    this.requestForm.controls.companyPhone.enable();
    console.log(this.requestForm.value);
    if(this.requestForm.status == "VALID"){
      this._deliveriesSvc.createDelivery(this.requestForm.value).then(data => data.json())
      .then((response) => {
        this.requestForm.controls.userId.disable();
        if (response.status == 200) {
          console.log(response);
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Solicitud de Entrega Creada',
            showConfirmButton: false,
            timer: 1500
          });
          this._router.navigate(['/business/dashboard']);
        }
        else {
          Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: response.message.code || response.message,
            showConfirmButton: false,
            timer: 1500
          });
          this.loading = false;
        }
        this.completeBarCharge();
      })
      .catch((err) => {
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: err,
          showConfirmButton: false,
          timer: 1500
        });
        this.completeBarCharge();
      });
    } else {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Verifique el formulario',
        showConfirmButton: false,
        timer: 1500
      });
      this.completeBarCharge();
    }
    
  }

  fillRequestForm(userId: string, userNames: string, userLocation: string, userPhone: string) {
    this.requestForm.controls.locationA.setValue(userLocation);
    this.requestForm.controls.userId.setValue(userId);
    this.requestForm.controls.userId.disable();
    this.requestForm.controls.companyName.setValue(userNames);
    this.requestForm.controls.companyName.disable();
    this.requestForm.controls.companyPhone.setValue(userPhone);
    this.requestForm.controls.companyPhone.disable();
  }

  startBarCharge() {
    this._loadingBarService.start();
    this.loading = true;
  }

  completeBarCharge() {
    this._loadingBarService.complete();
    this.loading = false;
  }


}
