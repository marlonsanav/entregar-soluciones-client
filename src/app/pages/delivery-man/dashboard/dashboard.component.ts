import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { DeliveriesService } from 'src/app/services/deliveries.service';
import { map } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { UsersService } from 'src/app/services/users.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-dashboard-delivery-man',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardDeliveryManComponent implements OnInit, OnDestroy, AfterViewInit {

  loading: boolean = false;

  loadingData: boolean = true;

  userId: string = '';

  user: any = {
    names: ""
  };

  subscriptionAllContingencies: Subscription = new Subscription();

  paginatorIntl = new MatPaginatorIntl();

  displayedColumns = ['start_location', 'delivery_location', 'product_name'];

  dataSourceX = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  @ViewChild(MatSort, { static: true })
  sort!: MatSort;

  constructor(private _router: Router, private _deliveriesSvc: DeliveriesService, private _usersSvc: UsersService) {
  }

  ngOnInit(): void {
    this.userId = localStorage.getItem('user_id') || '';
    this.getAllDeliveries();
    if(localStorage.getItem('user_names')){
      this.user.names = localStorage.getItem('user_names');
    }
  }

  ngOnDestroy(){
    this.subscriptionAllContingencies.unsubscribe();
  }

  ngAfterViewInit() {
    this.dataSourceX.paginator = this.paginator;
    this.dataSourceX.paginator._intl.itemsPerPageLabel = 'Items por página';
    this.dataSourceX.paginator._intl.nextPageLabel = 'Siguiente';
    this.dataSourceX.paginator._intl.previousPageLabel = 'Anterior';
    this.dataSourceX.paginator._intl.firstPageLabel = "Primera Página";
    this.dataSourceX.paginator._intl.lastPageLabel = "Última Página";
    this.dataSourceX.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length == 0 || pageSize == 0) {
        return `0 de ${length}`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      // If the start index exceeds the list length, do not try and fix the end index to the end.
      const endIndex = startIndex < length ?
        Math.min(startIndex + pageSize, length) :
        startIndex + pageSize;
      return `${startIndex + 1} – ${endIndex} de ${length}`;
    }
    this.dataSourceX.sort = this.sort;
  }

  getAllDeliveries() {
    this.loadingData = true;
    this.subscriptionAllContingencies = this._deliveriesSvc.getAllDeliveries().pipe(
      map(actions => {
        return actions.map(
          (data: any) => ({
            company_name: data.payload.doc.data().company_name,
            delivery_ID: data.payload.doc.id,
            delivery_price: new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(data.payload.doc.data().delivery_price),
            delivery_location: data.payload.doc.data().delivery_location,
            details: data.payload.doc.data().details,
            product_name: data.payload.doc.data().product_name,
            product_price: new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(data.payload.doc.data().product_price),
            product_type: data.payload.doc.data().product_type,
            start_location: data.payload.doc.data().start_location,
            user_ID: data.payload.doc.data().user_ID
          }));
      }))
      .subscribe(data => {
        this.loadingData = false;
        this.dataSourceX.data = data
      });
  }

  onAcceptRequest(data: any){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: true
    })

    swalWithBootstrapButtons.fire({
      title: 'Aceptar Solicitud',
      text: "¡Al aceptar esta solicitud deberá finalizarla para poder iniciar con una nueva!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, acepto',
      cancelButtonText: 'No, acepto',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        let dataToSend = {
          deliveryId: data.delivery_ID,
          deliveryManId: localStorage.getItem('user_id'),
          deliveryManName: localStorage.getItem('user_names'),
          deliveryManPhone: localStorage.getItem('user_phone')
        };      
        this._deliveriesSvc.takeDelivery(dataToSend).then( (res) => res.json())
        .then( (res) => {
          if(res.status == 'OK'){
            Swal.fire(
              'Confirmado',
              res.message,
              'success'
            );      
            this._router.navigate(['/delivery/delivery']);
          } else{
            Swal.fire(
              'Lo sentimos',
              res.message,
              'warning'
            );
          }          
        }).catch( (err) => {
          console.log(err);
          Swal.fire(
            'Error',
            'La solicitud tiene problemas para ser tomada',
            'error'
          );        
        });        
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Información',
          'Entendido, esta solicitud no será tomada',
          'info'
        );  
      }
    });
  }

}
