import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DeliveriesService } from 'src/app/services/deliveries.service';

@Component({
  selector: 'app-delivery-details',
  templateUrl: './delivery-details.component.html',
  styleUrls: ['./delivery-details.component.css']
})
export class DeliveryDetailsComponent implements OnInit, OnDestroy {

  deliveryId: any;
  delivery: any = [];

  subscriptionDelivery: Subscription = new Subscription();

  constructor(private _route: ActivatedRoute, private _deliveriesSvc: DeliveriesService) {
    this.deliveryId = this._route.snapshot.params.id;
   }

  ngOnInit(): void {
    this.getDeliveryById(this.deliveryId);
  }

  ngOnDestroy(): void {
      this.subscriptionDelivery.unsubscribe();
  }

  getDeliveryById(deilveryId: string){
    this.subscriptionDelivery = this._deliveriesSvc.getDeliveryById(deilveryId).subscribe((data:any) => {
      data.product_price = new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(data.product_price);
      data.delivery_price = new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(data.delivery_price);
      this.delivery = data;
    });
  }

}
