import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardDeliveryManComponent } from './dashboard/dashboard.component';
import { DeliveryComponent } from './delivery/delivery.component';
import { MovementsComponent } from './movements/movements.component';
import { DeliveryDetailsComponent } from './delivery-details/delivery-details.component';

import { AuthenticationGuard } from 'src/app/guard/authentication.guard';


const routes: Routes = [
  { path: '', redirectTo: 'dashboard'},
  { path: 'dashboard', component: DashboardDeliveryManComponent, canActivate: [AuthenticationGuard], data: {role: "delivery-man"} },
  { path: 'delivery', component: DeliveryComponent, canActivate: [AuthenticationGuard], data: {role: "delivery-man"} },
  { path: 'movements', component: MovementsComponent, canActivate: [AuthenticationGuard], data: {role: "delivery-man"} },
  { path: 'details/:id', component: DeliveryDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliveryManRoutingModule { }
