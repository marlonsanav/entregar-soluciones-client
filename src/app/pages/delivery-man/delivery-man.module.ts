import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardDeliveryManComponent } from './dashboard/dashboard.component';
import { DeliveryDetailsComponent } from './delivery-details/delivery-details.component';
import { DeliveryComponent } from './delivery/delivery.component';
import { MovementsComponent } from './movements/movements.component';

import { MaterialModule } from '../../utils/material/material.module';
import { ReactiveFormsModule  } from '@angular/forms';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { NgxBarcodeScannerModule } from "@eisberg-labs/ngx-barcode-scanner";

import { DeliveryManRoutingModule } from './delivery-man-routing.module';
import { DeliveriesService } from 'src/app/services/deliveries.service';



@NgModule({
  declarations: [
    DashboardDeliveryManComponent,
    DeliveryDetailsComponent,
    DeliveryComponent,
    MovementsComponent
  ],
  imports: [
    CommonModule,
    DeliveryManRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    LoadingBarModule,
    ZXingScannerModule,
    NgxBarcodeScannerModule,
  ],
  providers: [DeliveriesService]
})
export class DeliveryManModule { }
