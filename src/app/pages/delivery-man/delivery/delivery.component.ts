import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { DeliveriesService } from 'src/app/services/deliveries.service';
import { map } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { UsersService } from 'src/app/services/users.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.css']
})
export class DeliveryComponent implements OnInit, OnDestroy, AfterViewInit {

  loading: boolean = false;

  loadingData: boolean = true;

  userId: string = '';

  user: any = {
    names: ""
  };

  subscriptionAllDeliveriesTaken: Subscription = new Subscription();

  paginatorIntl = new MatPaginatorIntl();

  displayedColumns = ['company_name', 'product_name', 'product_price'];

  dataSourceX = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  @ViewChild(MatSort, { static: true })
  sort!: MatSort;

  constructor(private _router: Router, private _deliveriesSvc: DeliveriesService, private _usersSvc: UsersService) {
  }

  ngOnInit(): void {
    this.userId = localStorage.getItem('user_id') || '';
    this.getDeliveriesTakenByDeliveryManId();
    if(localStorage.getItem('user_names')){
      this.user.names = localStorage.getItem('user_names');
    }
  }

  ngOnDestroy(){
    this.subscriptionAllDeliveriesTaken.unsubscribe();
  }

  ngAfterViewInit() {
    this.dataSourceX.paginator = this.paginator;
    this.dataSourceX.paginator._intl.itemsPerPageLabel = 'Items por página';
    this.dataSourceX.paginator._intl.nextPageLabel = 'Siguiente';
    this.dataSourceX.paginator._intl.previousPageLabel = 'Anterior';
    this.dataSourceX.paginator._intl.firstPageLabel = "Primera Página";
    this.dataSourceX.paginator._intl.lastPageLabel = "Última Página";
    this.dataSourceX.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length == 0 || pageSize == 0) {
        return `0 de ${length}`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      // If the start index exceeds the list length, do not try and fix the end index to the end.
      const endIndex = startIndex < length ?
        Math.min(startIndex + pageSize, length) :
        startIndex + pageSize;
      return `${startIndex + 1} – ${endIndex} de ${length}`;
    }
    this.dataSourceX.sort = this.sort;
  }

  getDeliveriesTakenByDeliveryManId() {
    this.loadingData = true;
    this.subscriptionAllDeliveriesTaken = this._deliveriesSvc.getDeliveriesTakenByDeliveryManId(this.userId).pipe(
      map(actions => {
        return actions.map(
          (data: any) => ({
            company_name: data.payload.doc.data().company_name,
            company_phone: data.payload.doc.data().company_phone,
            delivery_ID: data.payload.doc.id,
            delivery_name: data.payload.doc.data().delivery_name,
            delivery_price: new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(data.payload.doc.data().delivery_price),
            delivery_phone: data.payload.doc.data().delivery_phone,
            delivery_location: data.payload.doc.data().delivery_location,
            details: data.payload.doc.data().details,
            product_name: data.payload.doc.data().product_name,
            product_price: new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(data.payload.doc.data().product_price),
            product_type: data.payload.doc.data().product_type,
            start_location: data.payload.doc.data().start_location,
            start_date: data.payload.doc.data().start_date,
            user_ID: data.payload.doc.data().user_ID,
            status: data.payload.doc.data().status
          }));
      }))
      .subscribe(data => {
        this.loadingData = false;
        this.dataSourceX.data = data
      });
  }

  onMarkRequest(data:any){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: true
    })

    swalWithBootstrapButtons.fire({
      title: '¡Gestionar!',
      text: "¿Como vas con el pedido?",
      
      showCancelButton: true,
      confirmButtonText: 'Ya lo recogí',
      cancelButtonText: 'No podré entregarlo',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this._deliveriesSvc.changeDeliveryStatusToGet(data.delivery_ID).then( (res) => {
          Swal.fire(
            'Confirmado',
            'La solicitud ha sido marcada como recogida',
            'success'
          )     
        })
        .catch( (err) => console.log(err));
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        this._deliveriesSvc.cancelDeliveryOnProccess(data.delivery_ID).then( (res) => {
          Swal.fire(
            'Confirmado',
            'La solicitud ha sido cancelada',
            'success'
          )     
        })
        .catch( (err) => console.log(err));
      }
    });
  }

  onFinishRequest(data:any){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: true
    })

    swalWithBootstrapButtons.fire({
      title: '¡Finalizar!',
      text: "¿Entregaste el pedido?",
      
      showCancelButton: true,
      confirmButtonText: 'Si, producto entregado',
      cancelButtonText: 'No, tuve una novedad',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this._deliveriesSvc.changeDeliveryStatusToFinish(data.delivery_ID).then( (res) => {
          Swal.fire(
            'Confirmado',
            'La solicitud ha sido marcada como finalizada',
            'success'
          )     
        })
        .catch( (err) => console.log(err));
      } else if (result.dismiss === Swal.DismissReason.cancel) {        
        this.onCommunicateDeliveryNew(data);         
      }
    });
  }

  async onCommunicateDeliveryNew(data:any){
    const { value: deliveryNew } = await Swal.fire({
      title: 'Seleccione una novedad',
      input: 'select',
      inputOptions: {
        'Novedad en Entrega': {
          optionOne: 'Cliente no contesta',
          optionTwo: 'Pedido incompleto',
          optionThree: 'Dirección errónea',
          optionFour: 'El Cliente no estaba',
          optionFive: 'Teléfono Incorrecto',
          optionSix: 'Teléfono Incorrecto'
        },
        'Otra Novedad': {
          optionSeven: 'Inconveniente en Vehículo',
          optionEight: 'Tráfico en Ciudad',
          optionNine: 'Otros'
        }
      },
      inputPlaceholder: 'Seleccione una novedad',
      showCancelButton: false,
      inputValidator: (value) => {
        return new Promise((resolve) => {
            this._deliveriesSvc.changeDeliveryStatusToInNew(data.delivery_ID, value).then( (res) => {
              setTimeout(() => {
                Swal.fire(
                  'Confirmado',
                  'La solicitud ha sido marcada como novedad',
                  'success'
                ); 
              }, 5000);
              resolve('Actualizando estado...');
            }).catch( (err) => {
              resolve('Ha ocurrido un error en el API');
            });   
        })
      }
    })
  }

  onContact(data: any){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: true
    })

    swalWithBootstrapButtons.fire({
      title: '¡Detalles del Pedido!',
      html: `<p style='font-size: 14px;text-align: justify;'>
             <b>Nombre del Cliente:</b> ${data.delivery_name}
             <br><b>Celular del Cliente:</b> ${data.delivery_phone}
             <br><b>Dirección del Cliente:</b> ${data.delivery_location}<br>
             <br><b>Nombre del Producto:</b> ${data.product_name}
             <br><b>Precio del Producto:</b> ${data.product_price}
             <br><b>Coste de Envío:</b> ${data.delivery_price}</p>`,
      showCancelButton: true,
      confirmButtonText: 'Llamar Empresa',
      cancelButtonText: 'Llamar Cliente ',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = 'tel://' + data.company_phone;
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        window.location.href = 'tel://' + data.delivery_phone;
      }
    });
  }

}
