import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { DeliveriesService } from 'src/app/services/deliveries.service';
import { map } from 'rxjs/operators';
import { UsersService } from 'src/app/services/users.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-movements',
  templateUrl: './movements.component.html',
  styleUrls: ['./movements.component.css']
})
export class MovementsComponent implements OnInit, OnDestroy {

  loading: boolean = false;

  loadingData: boolean = true;

  userId: string = '';

  user: any = {
    names: ""
  };

  movements: any;

  subscriptionAllMovements: Subscription = new Subscription();


  constructor(private _router: Router, private _deliveriesSvc: DeliveriesService, private _usersSvc: UsersService) {
  }

  ngOnInit(): void {
    this.userId = localStorage.getItem('user_id') || '';
    this.getDeliveriesByDeliveryManId();
    if(localStorage.getItem('user_names')){
      this.user.names = localStorage.getItem('user_names');
    }
  }

  ngOnDestroy(){
    this.subscriptionAllMovements.unsubscribe();
  }

  getDeliveriesByDeliveryManId() {
    this.loadingData = true;
    this.subscriptionAllMovements = this._deliveriesSvc.getDeliveriesEndsByDeliveryManId(this.userId).pipe(
      map(actions => {
        return actions.map(
          (data: any) => ({
            company_name: data.payload.doc.data().company_name,
            delivery_ID: data.payload.doc.id,
            delivery_price: new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(data.payload.doc.data().delivery_price),
            product_name: data.payload.doc.data().product_name,
            product_price: new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(data.payload.doc.data().product_price),
            start_date: data.payload.doc.data().start_date,
            status: data.payload.doc.data().status
          }));
      }))
      .subscribe(data => {
        this.loadingData = false;
        this.movements = data;
      });
  }

}
