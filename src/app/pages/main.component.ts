import { Component, OnInit } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { AuthenticationService } from '../services/authentication.service';
import { mergeMapTo } from 'rxjs/operators';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styles: [
  ]
})
export class MainComponent implements OnInit {

  userId: string;

  constructor(private _afMessaging: AngularFireMessaging, private _authSvc: AuthenticationService) {
    this.userId = localStorage.getItem("user_id") || "";
  }

  ngOnInit(): void {
    this.requestPermission();
  }

  requestPermission() {
    this._afMessaging.requestPermission
    .pipe(mergeMapTo(this._afMessaging.tokenChanges))
    .subscribe( 
        (token) => { 
          this._authSvc.createTokenNotifications(this.userId, token)
          .then(res => res.json())
          .then(data => console.log(data))
          .catch(err => console.log(err));
          console.log(token);
        },
        (error) => { 
          console.log(error);
        }
      ); 
  }

}
