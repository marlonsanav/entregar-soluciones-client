import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { first, map } from 'rxjs/operators';
import { UserInterface } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  urlEndpoints: string = 'https://entregarsoluciones.com.co/api/contingencies/';
  urlGlobalEndpoints: string = 'https://entregarsoluciones.com.co/api/';
  
  public userId: any;
  public userEmail: any;
  public userRole: any;
  public userLoginName: any;

  constructor(public _afAuth: AngularFireAuth, public _fStore: AngularFirestore) { }

  login(username: string, password: string) {

    return new Promise((resolve, reject) => {
      this._afAuth.setPersistence('local').then(() => {
        this._afAuth.signInWithEmailAndPassword(username, password).
          then(userData => {
            this.userId = userData.user?.uid;
            this.userEmail = userData.user?.email;
            localStorage.setItem('user_id', this.userId);
            localStorage.setItem('user_email', this.userEmail);
            resolve(userData);
          },
            err => reject(err))
      });
    });

  }

  loginWithClickLogin(username: string, password: string) {
    return new Promise((resolve, reject) => {
      this.userLoginName = this._fStore.collection<UserInterface>('users', ref => ref.where('login', '==', username)).valueChanges();
      this.userLoginName.pipe(        
          first()
        )                       
        .subscribe((data:any) => {

          if(data.length > 0){
            resolve(this.login(data[0].email, password));
          } else{
            reject("Error")
          }

        });
    });
   
  }

  logout() {
    localStorage.clear();    
    localStorage.removeItem('user_id');
    localStorage.removeItem('user_identification');
    localStorage.removeItem('user_names');
    localStorage.removeItem('user_location');
    localStorage.removeItem('user_email');
    localStorage.removeItem('user_role');
    return this._afAuth.signOut();
  }

  sendVerificationEmail(email: string): Promise<void> {
    return this._afAuth.sendPasswordResetEmail(email);
  }

  sendAccessRequest(data: any) {
    return fetch(this.urlGlobalEndpoints + 'access-request/create/',{
      method: 'POST',
      mode: 'cors',
      body: JSON.stringify(data),
      headers: {"Content-type": "application/json;charset=UTF-8"}
    });
  }

  verifyAccessRequest(userIdentification: any) {
    return fetch(this.urlGlobalEndpoints + 'access-request/verify/' + userIdentification,{
      method: 'GET',
      mode: 'cors',
      headers: {"Content-type": "application/json;charset=UTF-8"}
    });
  }

  createTokenNotifications(userId: any, token: any) {
    let data = {
      user_id: userId,
      token: token
    }
    return fetch(this.urlGlobalEndpoints + 'create-token/', {
      method: 'POST',
      mode: 'cors',
      body: JSON.stringify(data),
      headers: {"Content-type": "application/json;charset=UTF-8"}
    });
  }  

  getRole(userId: string) {
    return this._fStore.collection<UserInterface>('users', ref => ref.where('auth_ID', '==', userId)).valueChanges();
  }

  toStringRolesAndRoutes(profileId: string): string {
    switch (profileId) {
      case '1':
        profileId = "administrator";
        break;
      case '2':
        profileId = "delivery-man";
        break;
      case '3':
        profileId = "business";
        break;
    }
    return profileId;
  }

  toStringRoles(profileId: string): string {

    switch (profileId) {
      case '1':
        profileId = "Administrador";
        break;
      case '2':
        profileId = "Domiciliario";
        break;
      case '3':
        profileId = "Empresa";
        break;
    }
    return profileId;
  }

}
