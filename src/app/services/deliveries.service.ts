import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})

export class DeliveriesService {
    urlEndpoints: string = 'https://entregarsoluciones.com.co/api/delivery/';
    urlGlobalEndpoints: string = 'https://entregarsoluciones.com.co/api/';

    constructor(public _afStore: AngularFirestore, public _afAuth:AngularFireAuth) { }
    
    getDeliveries(userId: string){
        const today = new Date();
        today.setHours(0,0,0,0);
        return this._afStore.collection("deliveries", ref => ref.where('user_ID','==', userId).where('start_date','>', today).orderBy('start_date','asc')).snapshotChanges();
    }

    getAllDeliveries(){
        return this._afStore.collection("deliveries", ref => ref.where('status','==', 0).orderBy('start_date','asc')).snapshotChanges();
    }

    getAllDeliveriesById(userId: string){
        return this._afStore.collection("deliveries", ref => ref.where('user_ID','==', userId).orderBy('contingencies_Date','desc')).snapshotChanges();
    }

    getDeliveriesByCompanyId(companyId: string){
        const today = new Date();
        today.setHours(0,0,0,0);
        return this._afStore.collection("deliveries", ref => ref.where('user_ID','==', companyId).where('status','in', [0,1,2,3,4]).where('start_date','>', today).orderBy('start_date','desc')).snapshotChanges();
    }

    getDeliveriesEndsByCompanyId(companyId: string){
        return this._afStore.collection("deliveries", ref => ref.where('user_ID','==', companyId).where('status','==', 3).orderBy('start_date','desc')).snapshotChanges();
    }

    getDeliveryById(deliveryId: string){
        return this._afStore.collection("deliveries").doc(deliveryId).valueChanges();
    }

    getDeliveriesEndsByDeliveryManId(userId: string){
        return this._afStore.collection("deliveries", ref => ref.where('delivery_man_ID', '==', userId).where('status','==', 3).orderBy('start_date', 'desc').limit(25)).snapshotChanges();
    }

    getDeliveriesTakenByDeliveryManId(userId: string){
        return this._afStore.collection("deliveries", ref => ref.where('delivery_man_ID', '==', userId).where('status','in', [0,1,2,3,4]).orderBy('start_date', 'desc').limit(25)).snapshotChanges();
    }

    createDelivery(data: any){
        return fetch(this.urlEndpoints,{
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify(data),
            headers: {"Content-type": "application/json;charset=UTF-8"}
          });    
    }

    takeDelivery(data: any){
        return fetch(this.urlGlobalEndpoints + 'take-delivery', {
            method: 'PUT',
            mode: 'cors',
            body: JSON.stringify(data),
            headers: {"Content-type": "application/json;charset=UTF-8"}
        });
    }

    cancelDeliveryOnProccess(deliveryId: string){
        return this._afStore.collection("deliveries").doc(deliveryId).update({status: 0, delivery_man_ID: "", delivery_man_name: "", delivery_man_phone: ""});
    }

    changeDeliveryStatusToGet(deliveryId: string){
        return this._afStore.collection("deliveries").doc(deliveryId).update({status: 2});
    }

    changeDeliveryStatusToInNew(deliveryId: string, comment: string){
        return this._afStore.collection("deliveries").doc(deliveryId).update({status: 4, delivery_man_comments: comment});
    }

    changeDeliveryStatusToFinish(deliveryId: string){
        return this._afStore.collection("deliveries").doc(deliveryId).update({status: 3});
    }

    cancelDelivery(deliveryId: string){
        return this._afStore.collection("deliveries").doc(deliveryId).update({status: 4});
    }

    sendPushNotification(userToSend: string, messageType: string){
        return fetch(this.urlGlobalEndpoints + "notification/" + messageType + "/" + userToSend, {
            method: "GET",
            mode: "cors",
            cache: 'no-cache',
            referrerPolicy: 'no-referrer',
            headers: {"Content-type": "application/json;charset=UTF-8"}
        });
    }
}