import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  urlEndpoints: string = 'https://entregarsoluciones.com.co/api/users/';
  urlGlobalEndpoints: string = 'https://entregarsoluciones.com.co/api/';

  constructor(public _afStore: AngularFirestore) { }

  getAllUsers() {
    return this._afStore.collection("users").snapshotChanges();
  }

  getUserById(userId: string) {
    return this._afStore.collection("users").doc(userId).valueChanges();
  }

  getCurrentUser(userId: string) {
    return this._afStore.collection("users", ref => ref.where('auth_ID', '==', userId)).snapshotChanges();
  }

  getUserByDeliveryId(deliveryId: string) {
    return fetch(this.urlGlobalEndpoints + "user/delivery/" + deliveryId, {
      method: 'GET',
      mode: 'cors',
      headers: {"Content-type": "application/json;charset=UTF-8"}
    });
  }

  createUser(data: any) {
    return fetch(this.urlEndpoints + 'create-user',{
      method: 'POST',
      body: JSON.stringify(data),
      headers: {"Content-type": "application/json;charset=UTF-8"}
    });
  }
    
  createCompany(data: any) {
    return fetch(this.urlEndpoints + 'create-company',{
      method: 'POST',
      body: JSON.stringify(data),
      headers: {"Content-type": "application/json;charset=UTF-8"}
    });
  }

  updateUser(data: any) {
    return fetch(this.urlEndpoints + 'update-user/',{
      method: 'PUT',
      mode: 'cors',
      body: JSON.stringify(data),
      headers: {"Content-type": "application/json;charset=UTF-8"}
    });
  }

  deleteUser(data: any) {
    data.status = 0;

    return new Promise((resolve, reject) => {
      this._afStore.collection("users").doc(data.userId).update({status: data.status})
        .then(
          (res) => resolve(res),
          (err) => reject(err)
        );
        
    });
  }

  changeUserStatus<Promise>(userId: string, status: string){
    let data = {
      user_id: userId,
      status: status == 'Activo' ? 0 : 1
    }
    return fetch(this.urlGlobalEndpoints + 'user/change-status/', {
      method: 'PUT',
      mode: 'cors',
      body: JSON.stringify(data),
      headers: {"Content-type": "application/json;charset=UTF-8"}
    });
  }

}

