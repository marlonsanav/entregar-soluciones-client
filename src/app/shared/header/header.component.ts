import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UsersService } from 'src/app/services/users.service';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  isMenuOpen: boolean = false;
  userRole: any;

  subscriptionCurrentUser: Subscription = new Subscription();


  constructor(private _fAuth: AuthenticationService, private _router: Router, private _usersSvc: UsersService) {
    const userId = localStorage.getItem('user_id') || '';
    this.getCurrentUserById(userId);
  }

  
  ngOnInit(): void {
    
  }

  ngOnDestroy(): void {
      this.subscriptionCurrentUser.unsubscribe();
  }

  onLogout(){
    this._fAuth.logout();
    this._router.navigate(['/login']);
  }

  getCurrentUserById(userId: string){
    this.subscriptionCurrentUser = this._usersSvc.getCurrentUser(userId).pipe(
      map(actions => {
        return actions.map(
          (data: any) => ({
            names: data.payload.doc.data().names,
            identification: data.payload.doc.data().identification,
            phone: data.payload.doc.data().phone,
            profile_ID: data.payload.doc.data().profile_ID,
            location: data.payload.doc.data().location
          }));
      }))
      .subscribe(res => {
        console.log(res);
        this.userRole = res[0].profile_ID;
        localStorage.setItem('user_names', res[0].names);
        localStorage.setItem('user_identification', res[0].identification);
        localStorage.setItem('user_role', res[0].profile_ID);
        localStorage.setItem('user_location', res[0].location);
        localStorage.setItem('user_phone', res[0].phone);
      });
  }

  onOpenMenu(event:any){
    const elementSideNav = document.getElementById('sidebarMenu') || '';
    if(elementSideNav != ''){
      if(this.isMenuOpen){
        elementSideNav.classList.remove("open");
        this.isMenuOpen = false;
      }
      else{
        elementSideNav.classList.add("open");
        this.isMenuOpen = true;
      }
    }
  }
}
