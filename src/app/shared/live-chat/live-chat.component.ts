import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { DeliveriesService } from 'src/app/services/deliveries.service';
import { UsersService } from 'src/app/services/users.service';

export interface ContingencyInterface {
  contingency_ID?: string,
  user_ID: string;
  contingency_Type: string;
  contingency_State: string;
  contingency_Date: Date;
  answer: string;
  details: string;
  reference: string;
}

@Component({
  selector: 'app-live-chat',
  templateUrl: './live-chat.component.html',
  styleUrls: ['./live-chat.component.css']
})
export class LiveChatComponent implements OnInit{

  loading: boolean = false;

  loadingData: boolean = true;

  userId: string = '';

  user: any = {
    names: ""
  };
  constructor(private _router: Router, private _deliveriesSvc: DeliveriesService, private _usersSvc: UsersService) {
  }

  ngOnInit(): void {
    this.userId = localStorage.getItem('user_id') || '';
    if(localStorage.getItem('user_names')){
      this.user.names = localStorage.getItem('user_names');
    }
  }


}
