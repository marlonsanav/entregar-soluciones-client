import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { UsersService } from 'src/app/services/users.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {

  userRole:number = 0;

  constructor(private _router: Router,private _usersSvc: UsersService) { }

  ngOnInit(): void {
    const userId = localStorage.getItem('user_id') || '';
    this.getRoleById(userId);
  }

  getRoleById(userId: string){
    this._usersSvc.getCurrentUser(userId).pipe(
      map(actions => {
        return actions.map(
          (data: any) => ({
            profile_ID: data.payload.doc.data().profile_ID
          }));
      }))
      .subscribe(res => {this.userRole = res[0].profile_ID});
  }
  
}
