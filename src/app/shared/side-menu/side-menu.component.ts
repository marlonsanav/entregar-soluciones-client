import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UsersService } from 'src/app/services/users.service';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {

  userId: any;

  userName: any = "Por Defecto";

  userRole:number = 0;

  userRoleName: string = '';

  subscriptionCurrentUser: Subscription = new Subscription();

  constructor(private _router: Router,private _usersSvc: UsersService, private _authSvc: AuthenticationService) { }

  ngOnInit(): void {
    this.userId = localStorage.getItem('user_id') || '';
    this.userName = localStorage.getItem('user_names') || '';
    this.getRoleById(this.userId);
  }

  ngOnDestroy(): void {
    this.subscriptionCurrentUser.unsubscribe();
  }

  onNavigate(link:string){
    this._router.navigate([link]);
    const elementHamburger = document.getElementById('openSidebarMenu') || '';
    if(elementHamburger != ''){
      elementHamburger.click();
    }
  }  

  getRoleById(userId: string){
    this.subscriptionCurrentUser = this._usersSvc.getCurrentUser(userId).pipe(
      map(actions => {
        return actions.map(
          (data: any) => ({
            names: data.payload.doc.data().names,
            profile_ID: data.payload.doc.data().profile_ID
          }));
      }))
      .subscribe(res => {
        this.userName = res[0].names;
        this.userRole = res[0].profile_ID;
        this.userRoleName = this._authSvc.toStringRoles(this.userRole.toString());
      });
  }

  onLogout(){
    this._authSvc.logout();
    this._router.navigate(['/login']);
  }
}
