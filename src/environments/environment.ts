// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAP9KAhdTGDDAKrFP6fISgJcvb66HGqHOg",
    authDomain: "entregar-soluciones.firebaseapp.com",
    databaseURL: 'https://entregar-soluciones.firebaseio.com',
    projectId: "entregar-soluciones",
    storageBucket: "entregar-soluciones.appspot.com",
    messagingSenderId: "185155529518",
    appId: "1:185155529518:web:702aebc1607f11eed5e8bf",
    measurementId: "G-63WGKJZFWZ"
  }
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
