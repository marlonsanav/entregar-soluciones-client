// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/8.7.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.7.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.

firebase.initializeApp({
    apiKey: "AIzaSyAP9KAhdTGDDAKrFP6fISgJcvb66HGqHOg",
    authDomain: "entregar-soluciones.firebaseapp.com",
    databaseURL: 'https://entregar-soluciones.firebaseio.com',
    projectId: "entregar-soluciones",
    storageBucket: "entregar-soluciones.appspot.com",
    messagingSenderId: "185155529518",
    appId: "1:185155529518:web:702aebc1607f11eed5e8bf",
    measurementId: "G-63WGKJZFWZ"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();